# Official PHP image
FROM php:7.2-fpm
# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
# Install required extensions
RUN docker-php-ext-install pdo_mysql
# Install other requirements
RUN apt-get update \
    && apt-get install -y git unzip
# Speed up composer
RUN composer global require hirak/prestissimo