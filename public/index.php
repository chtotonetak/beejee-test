<?php

use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../app/bootstrap.php';

// Handle request
$request = Request::createFromGlobals();
$response = app()->handle($request);
$response->send();