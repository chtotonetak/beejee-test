<?php


namespace App\Models;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tasks")
 */
class Task extends Model
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $email;

    /**
     * @ORM\Column(type="text")
     */
    protected $text;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    protected $isDone;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    protected $isEdited;

    public function __construct()
    {
        $this->setIsDone(false);
        $this->setIsEdited(false);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getIsEdited()
    {
        return $this->isEdited;
    }

    /**
     * @param mixed $isEdited
     */
    public function setIsEdited($isEdited)
    {
        $this->isEdited = $isEdited;
    }

    /**
     * @return mixed
     */
    public function isDone()
    {
        return $this->isDone;
    }

    /**
     * @param mixed $isDone
     */
    public function setIsDone($isDone)
    {
        $this->isDone = $isDone;
    }

    public static function getByPage($pageNumber, $countPerPage, $sortArray)
    {
        $qb = self::getEntityManager()->createQueryBuilder();
        $qb->select('u')
            ->from(self::class, 'u');

        if (!empty($sortArray)) {
            $qb->orderBy(
                join('.', ['u', array_key_first($sortArray)]),
                array_values($sortArray)[0]
            );
        }

        // Apply limit & offset
        $qb->setMaxResults($countPerPage)
            ->setFirstResult(($pageNumber - 1) * $countPerPage);

        return $qb->getQuery()->getResult();
    }
}