<?php


namespace App\Models;


use Doctrine\ORM\ORMException;

class Model
{
    public static function getEntityManager()
    {
        return app()->getDb()->em();
    }

    public static function getRepository()
    {
        return self::getEntityManager()->getRepository(static::class);
    }

    public static function getAll()
    {
        return self::getRepository()->findAll();
    }

    public static function count()
    {
        return self::getRepository()->count([]);
    }

    public static function findBy($field, $value)
    {
        return self::getRepository()->findOneBy([$field => $value]);
    }

    public function save()
    {
        try {
            app()->getDb()->em()->persist($this);
            app()->getDb()->em()->flush();
        } catch (ORMException $ex) {
            // Log somewhere
        }
    }
}