<?php


namespace App\Core;


use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Renderer
{
    private $_twig;

    /**
     * Init Twig
     */
    public function boot()
    {
        $loader = new FilesystemLoader(__DIR__ . '/../templates');
        $this->_twig = new Environment($loader);
    }

    /**
     * Render template
     *
     * @param $template
     * @param $context
     * @return mixed
     */
    public function render($template, $context)
    {
        return $this->_twig->render($template, $context);
    }

    /**
     * Add variable accessible across the all templates
     *
     * @param $name
     * @param $value
     */
    public function addGlobalVariable($name, $value)
    {
        $this->_twig->addGlobal($name, $value);
    }
}