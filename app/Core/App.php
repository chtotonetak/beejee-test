<?php


namespace App\Core;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class App
{
    /** @var App singleton object */
    private static $_app;

    /** @var Router */
    private $_router;

    /** @var Database */
    private $_db;

    /** @var Renderer */
    private $_renderer;

    /** @var Auth */
    private $_auth;

    public function __construct()
    {
        $this->_db = new Database();
        $this->_router = new Router();
        $this->_renderer = new Renderer();
        $this->_auth = new Auth();
    }

    /**
     * @return Database
     */
    public function getDb()
    {
        return $this->_db;
    }

    /**
     * @return Renderer
     */
    public function getRenderer()
    {
        return $this->_renderer;
    }

    /**
     * @return Auth
     */
    public function getAuth()
    {
        return $this->_auth;
    }

    /**
     * @return App singleton
     */
    public static function getInstance()
    {
        if (empty(self::$_app)) {
            self::$_app = new App();
        }

        return self::$_app;
    }

    /**
     * Load necessary parts of application
     */
    public function boot()
    {
        $this->_renderer->boot();
        $this->_db->boot();
        $this->_auth->boot();

        $this->_renderer->addGlobalVariable('siteName', 'MVC Application');
        $this->_renderer->addGlobalVariable('isAdmin', $this->_auth->isAuthenticated());
    }

    /**
     * Starts application by routing requests
     * @param Request $request
     * @return Response
     */
    public function handle($request)
    {
        return $this->_router->handle($request);
    }
}