<?php


namespace App\Core;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;

class Database
{
    /**
     * @var EntityManager
     */
    private $_em;

    /**
     * Init database connection
     */
    public function boot()
    {
        try {
            $isDevMode = true;
            $proxyDir = null;
            $cache = null;
            $useSimpleAnnotationReader = false;
            $config = Setup::createAnnotationMetadataConfiguration(array(__DIR__ . '/../Models/'), $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

            // Database configuration parameters
            $connectionParams = array(
                'dbname' => $_ENV['DB_DATABASE'],
                'user' => $_ENV['DB_USERNAME'],
                'password' => $_ENV['DB_PASSWORD'],
                'host' => $_ENV['DB_HOST'],
                'driver' => 'pdo_mysql',
            );

            // Obtaining the entity manager
            $this->_em = EntityManager::create($connectionParams, $config);
        } catch (ORMException $e) {
            // Log somewhere
        }
    }

    /**
     * Public getter
     * @return EntityManager
     */
    public function em()
    {
        return $this->_em;
    }
}