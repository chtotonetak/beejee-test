<?php


namespace App\Core;

use App\Controllers\AuthController;
use App\Controllers\Controller;
use App\Controllers\HomeController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


class Router
{
    /**
     * Boot router object with predefined routes.
     * @param Request $request
     * @return mixed|Response
     */
    public function handle($request)
    {
        $response = new Response();
        // Define routes
        $routes = array(
            // Main page
            'index' => (new Route('/{page}', ['_controller' => HomeController::class]))
                ->addDefaults(['page' => 1])
                ->addRequirements(['page' => '\d+']),
            // Create or update task
            'createOrUpdateTask' => (new Route('/edit/{id?}', ['_controller' => HomeController::class]))
                ->addRequirements(['id' => '\d+']),
            // Login
            'login' => new Route('/login', ['_controller' => AuthController::class]),
            // Logout
            'logout' => new Route('/logout', ['_controller' => AuthController::class]),
        );

        // Add routes
        $routesCollection = new RouteCollection();
        foreach ($routes as $name => $route) {
            $routesCollection->add($name, $route);
        }

        // Init RequestContext object
        $context = new RequestContext();
        $context->fromRequest($request);

        try {
            // Init UrlMatcher object
            $matcher = new UrlMatcher($routesCollection, $context);
            $routeInfo = $matcher->match($context->getPathInfo());

            // Call method
            $controller = [$routeInfo['_controller'], $routeInfo['_route']];
            unset($routeInfo['_controller'], $routeInfo['_route']);
            return call_user_func($controller, $request, $routeInfo);
        } catch (ResourceNotFoundException $exception) {
            // Log somewhere
            return Controller::error404();
        }
    }
}