<?php


namespace App\Core;


class Auth
{
    /** @var integer */
    private $_userId;

    /** @var boolean */
    private $_isAuthenticated;

    /**
     * Retrieve user from session data
     */
    public function boot()
    {
        session_start();
        if (isset($_SESSION['user_id'])) {
            $this->_userId = $_SESSION['user_id'];
            $this->_isAuthenticated = true;
        }
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->_userId;
    }

    /**
     * @return mixed
     */
    public function isAuthenticated()
    {
        return $this->_isAuthenticated;
    }

    /**
     * User auth
     * @param $userId
     */
    public function authenticate($userId)
    {
        $_SESSION['user_id'] = $userId;
        $this->_userId = $userId;
        $this->_isAuthenticated = true;
    }

    /**
     * User de-auth
     */
    public function logout()
    {
        $_SESSION['user_id'] = null;
        $this->_userId = null;
        $this->_isAuthenticated = false;
    }
}