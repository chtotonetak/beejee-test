<?php


namespace App\Controllers;


use App\Models\User;
use Symfony\Component\HttpFoundation\Request;

class AuthController extends Controller
{
    /**
     * Auth user
     *
     * @param Request $request
     * @return mixed
     */
    public static function login($request)
    {
        if (app()->getAuth()->isAuthenticated()) {
            self::goHome();
        }

        if ($request->getMethod() === 'GET') {
            return self::view('sign-in.twig');
        }

        // Validate request
        $validationResult = self::validateLoginRequest($request);

        if (empty($validationResult['errors'])) {
            // Try to auth
            $user = User::findBy('login', $request->get('login'));
            if (!empty($user)) {
                // Check password
                $passwordHash = $user->getPassword();
                if (password_verify($request->get('password'), $passwordHash)) {
                    app()->getAuth()->authenticate($user->getId());
                    self::goHome();
                }
            }
            // Unable to find user
            $validationResult['errors']['sign_in'] = 'Unable to find a user with provided login and password.';
        }

        // Show errors
        return self::view('sign-in.twig', [
            'errors' => $validationResult['errors'],
            'oldValues' => $validationResult['oldValues']
        ]);
    }


    /**
     * Logout user
     */
    public static function logout()
    {
        app()->getAuth()->logout();
        self::goHome();
    }

    /**
     * Validate request
     *
     * @param $request
     * @return array
     */
    private static function validateLoginRequest($request)
    {
        $errors = [];
        $oldValues = [];

        // Login
        $oldValues['login'] = $request->get('login');
        if (empty($request->get('login'))) {
            $errors['login'] = 'You have to provide a login';
        }

        // Password
        if (empty($request->get('password'))) {
            $errors['password'] = 'You have to provide a password';
        }

        return [
            'errors' => $errors,
            'oldValues' => $oldValues
        ];
    }
}