<?php


namespace App\Controllers;



use Symfony\Component\HttpFoundation\Response;

class Controller
{
    /**
     * Render template
     *
     * @param $template
     * @param $context
     * @return mixed
     */
    public static function view($template, $context = [])
    {
        return (new Response())->setContent(app()->getRenderer()->render($template, $context));
    }

    /**
     * Show 4040 error
     *
     * @return mixed
     */
    public static function error404()
    {
        return self::view('404.twig');
    }

    /**
     * Simple redirect to the main page
     */
    public static function goHome()
    {
        header("Location: /");
        die();
    }

    /**
     * Simple redirect to the login page
     */
    public static function goToLoginPage()
    {
        header("Location: /login");
        die();
    }
}