<?php


namespace App\Controllers;


use App\Models\Task;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    const TASKS_PER_PAGE = 3;

    // Sort type constants
    const ORDER_NAME_ASC = 0;
    const ORDER_NAME_DESC = 1;
    const ORDER_EMAIL_ASC = 2;
    const ORDER_EMAIL_DESC = 3;
    const ORDER_IS_DONE_FIRST = 4;
    const ORDER_IS_DONE_LAST = 5;

    /**
     * Show tasks
     *
     * @param Request $request
     * @param array $routeData
     * @return Response
     */
    public static function index($request, $routeData)
    {
        // Calculated pages related data
        $tasksCount = Task::count();
        $pagesCount = (int)($tasksCount / self::TASKS_PER_PAGE);
        if ($tasksCount % self::TASKS_PER_PAGE !== 0 || $pagesCount == 0) $pagesCount++;
        $page = $routeData['page'] > $pagesCount || $routeData['page'] == 0 ? 1 : $routeData['page'];

        // Retrieve sorting order
        $sortArray = ['name' => 'asc'];
        if (!empty($sort = $request->get('sort'))) {
            switch ($sort) {
                case self::ORDER_NAME_DESC:
                    $sortArray = ['name' => 'desc'];
                    break;
                case self::ORDER_EMAIL_ASC:
                    $sortArray = ['email' => 'asc'];
                    break;
                case self::ORDER_EMAIL_DESC:
                    $sortArray = ['email' => 'desc'];
                    break;
                case self::ORDER_IS_DONE_FIRST:
                    $sortArray = ['isDone' => 'desc'];
                    break;
                case self::ORDER_IS_DONE_LAST:
                    $sortArray = ['isDone' => 'asc'];
                    break;
            }
        }

        // Query tasks
        $tasks = Task::getByPage($page, self::TASKS_PER_PAGE, $sortArray);

        return self::view('index.twig', [
            'page' => $page,
            'pagesCount' => $pagesCount,
            'tasks' => $tasks,
            'sort' => $request->get('sort'),
        ]);
    }

    /**
     * Create or update task
     *
     * @param Request $request
     * @param array $routeData
     * @return mixed
     */
    public static function createOrUpdateTask($request, $routeData)
    {
        // Check if it is update request
        // Probe existence of id variable
        if (!empty($taskId = $routeData['id'])) {
            $isUpdateRequest = true;
            // Check if admin
            if (!app()->getAuth()->isAuthenticated()) {
                self::goToLoginPage();
            }
            $task = Task::findBy('id', $taskId);
            if (empty($task)) {
                return self::error404(); // Can't find task with provided id
            }
            // Fill values
            $oldValues['name'] = $task->getName();
            $oldValues['email'] = $task->getEmail();
            $oldValues['text'] = $task->getText();
            $oldValues['isDone'] = $task->isDone();
        }

        if ($request->getMethod() === 'GET') {
            return self::view('edit.twig', compact('oldValues'));
        }

        // Validate request
        $validationResult = self::validateTaskRequest($request);

        if (empty($validationResult['errors'])) {
            // Add task
            if (empty($task)) {
                $task = new Task();
            }

            // Update specific fields
            if (!empty($isUpdateRequest)) {
                $task->setIsEdited($task->getText() !== $request->get('text'));
                $task->setIsDone($request->get('is_done') === 'on');
                $message = 'Congrats! The Task Was Edited!';
            } else {
                $message = 'Congrats! A New Task Was Added!';
            }
            $task->setName($request->get('name'));
            $task->setEmail($request->get('email'));
            $task->setText($request->get('text'));
            $task->save();

            // Show success message
            return self::view('edit-success.twig', compact('message'));
        } else {
            // Show errors
            return self::view('edit.twig', [
                'errors' => $validationResult['errors'],
                'oldValues' => $validationResult['oldValues']
            ]);
        }
    }

    /**
     * Validate task create / update request
     *
     * @param $request
     * @return array
     */
    private static function validateTaskRequest($request)
    {
        $errors = [];
        $oldValues = [];

        // Name
        $oldValues['name'] = $request->get('name');
        if (empty($request->get('name'))) {
            $errors['name'] = 'You have to provide a valid task name';
        }

        // Email
        $oldValues['email'] = $request->get('email');
        if (empty($request->get('email')) ||
            !filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = 'You have to provide a valid email address';
        }

        // Text
        $oldValues['text'] = $request->get('text');
        if (empty($request->get('text'))) {
            $errors['text'] = 'You have to provide a text for the task';
        }

        return [
            'errors' => $errors,
            'oldValues' => $oldValues
        ];
    }
}
