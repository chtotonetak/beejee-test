<?php

use App\Core\App;

require __DIR__ . '/../vendor/autoload.php';

function app()
{
    return App::getInstance();
}

app()->boot();
