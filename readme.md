## Test Project for BeeJee

### Simple MVC task-manager

Execute inside container to create database schema
```
vendor/bin/doctrine orm:schema-tool:update --force --dump-sql
```

Execute inside container to seed admin user
```
php seed-user.php
```